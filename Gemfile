# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.3.1'

gem 'bootstrap-sass', '~> 3.3.6'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.0.rc2'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

gem 'coffee-rails', '~> 4.2'
gem 'kaminari'
gem 'redis', '~> 3.0'
gem 'sidekiq', '~> 5.0.0.rc1'
gem 'airbrake-ruby'

group :development, :test do
  gem 'capybara', '~> 2.13.0'
  gem 'rspec-rails', '~> 3.5'
  gem 'selenium-webdriver'
end

group :test do
  gem 'factory_girl_rails'
  gem 'rails-controller-testing'
  gem 'simplecov', require: false
end

group :development do
  gem 'foreman', '~> 0.84.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'pry', require: false
  gem 'rubocop', require: false
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
