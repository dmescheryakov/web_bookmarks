# frozen_string_literal: true

FactoryGirl.define do
  factory :bookmark do
    title 'Cats on reddit.'
    url 'https://reddit.com/cats'
    shortening 'cats'
  end

  factory :tag do
    title 'cats'
  end
end
