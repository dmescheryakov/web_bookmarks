# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Manage bookmarks' do
  let(:bookmark_attrs) do
    ['Cats on reddit', 'https://reddit.com/cats', 'cats']
  end

  scenario 'create a new bookmark' do
    create_bookmark(*bookmark_attrs)
    bookmark_attrs.each do |attr|
      expect(page).to have_content(attr)
    end
  end

  scenario 'delete a bookmark' do
    create_bookmark(*bookmark_attrs)
    visit root_path
    click_on 'Delete'
    expect(page).to_not have_content(bookmark_attrs[0])
  end

  scenario 'update a bookmark' do
    create_bookmark(*bookmark_attrs)
    click_on 'Edit'
    fill_in 'bookmark[title]', with: 'Dogs on reddit'
    fill_in 'bookmark[url]', with: 'https://reddit.com/dogs'
    fill_in 'bookmark[shortening]', with: 'dogs'
    click_on 'Update Bookmark'

    ['Dogs on reddit', 'https://reddit.com/dogs', 'dogs'].each do |attr|
      expect(page).to have_content(attr)
    end
  end

  def create_bookmark(title, url, shortening)
    visit new_bookmark_path

    fill_in 'bookmark[title]', with: title
    fill_in 'bookmark[url]', with: url
    fill_in 'bookmark[shortening]', with: shortening
    click_on 'Create Bookmark'
  end
end
