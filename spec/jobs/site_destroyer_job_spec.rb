# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SiteDestroyerJob, type: :job do
  include ActiveJob::TestHelper

  let(:site_id) { 9987 }
  let(:bookmark_id) { 667 }
  subject(:job) { SiteDestroyerJob.perform_later(site_id, bookmark_id) }

  it 'queues the job' do
    expect do
      job
    end.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(SiteDestroyerJob.new.queue_name).to eq('default')
  end

  it 'executes perform' do
    expect(SiteBuilder).to receive(:destroy_site_if_no_references)
        .with(site_id, bookmark_id)
    perform_enqueued_jobs { job }
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
