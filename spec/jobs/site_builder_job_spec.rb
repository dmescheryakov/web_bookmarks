# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SiteBuilderJob, type: :job do
  include ActiveJob::TestHelper

  let(:url) { 'https://reddit.com/cats' }
  let(:bookmark_id) { 1234 }
  subject(:job) { SiteBuilderJob.perform_later(url, bookmark_id) }

  it 'queues the job' do
    expect do
      job
    end.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(SiteBuilderJob.new.queue_name).to eq('default')
  end

  it 'executes perform' do
    expect(SiteBuilder).to receive(:call).with(url, bookmark_id)
    perform_enqueued_jobs { job }
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
