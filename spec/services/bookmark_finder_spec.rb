# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BookmarkFinder do
  describe '.search' do
    it 'searches bookmark by any attributes' do
      FactoryGirl.create(:bookmark, title: 'Cats on reddit.',
                                    url: 'https://reddit.com/cats',
                                    shortening: 'cats')
      FactoryGirl.create(:bookmark, title: 'Dogs on reddit.',
                                    url: 'https://reddit.com/dogs',
                                    shortening: 'dogs')
      FactoryGirl.create(:bookmark, title: 'Dogs in the internet.',
                                    url: 'https://google.com/',
                                    shortening: 'google')

      result = BookmarkFinder.search(term: 'cats')
      expect(result.count).to eql(1)
      expect(result[0].title).to eql('Cats on reddit.')

      result = BookmarkFinder.search(term: 'reddit')
      expect(result.count).to eql(2)
      expect(result[0].title).to eql('Cats on reddit.')
      expect(result[1].title).to eql('Dogs on reddit.')
    end
  end

  describe '.search_by_tag' do
    it 'searches bookmark by related tag' do
      bookmark_1 = FactoryGirl.create(:bookmark, title: 'Cats on reddit.',
                                                 url: 'https://reddit.com/cats',
                                                 shortening: 'cats')
      bookmark_2 = FactoryGirl.create(:bookmark, title: 'Dogs on reddit.',
                                                 url: 'https://reddit.com/dogs',
                                                 shortening: 'dogs')
      tag_1 = FactoryGirl.create(:tag, title: 'cats')
      tag_2 = FactoryGirl.create(:tag, title: 'dogs')
      BookmarkTag.create(bookmark_id: bookmark_1.id, tag_id: tag_1.id)
      BookmarkTag.create(bookmark_id: bookmark_2.id, tag_id: tag_2.id)
      result = BookmarkFinder.search(tag: 'cats')
      expect(result.count).to eql(1)
      expect(result[0].title).to eql('Cats on reddit.')
    end
  end
end
