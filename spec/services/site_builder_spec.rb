# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SiteBuilder do
  let(:url) { 'https://reddit.com/cats' }
  let(:bookmark_id) { 987 }

  describe '.call' do
    it 'initializes a new instance of SiteBuilder and calls "call" on it' do
      builder = spy
      expect(SiteBuilder).to receive(:new)
        .with(url, bookmark_id).and_return(builder)
      SiteBuilder.call(url, bookmark_id)
      expect(builder).to have_received(:call)
    end
  end

  describe '#call' do
    context 'when there is no site for giving url' do
      it 'creates a new site for giving url and update related bookmark' do
        site = double(id: 9876)
        bookmark = spy
        top_level_url = 'https://reddit.com/'
        expect(Site).to receive(:find_or_create_by).with(url: top_level_url)
          .and_return(site)
        expect(Bookmark).to receive(:find).with(bookmark_id)
          .and_return(bookmark)
        builder = SiteBuilder.new(url, bookmark_id)
        builder.call
        expect(bookmark).to have_received(:update).with(site_id: site.id)
      end
    end
  end

  describe '.destroy_site_if_no_references' do
    let(:site_id) { 998 }
    let(:bookmark_id) { 618 }

    context 'when there is no references to site anymore' do
      it 'destroyes the site' do
        site = spy
        expect(Site).to receive(:find_by).with(id: site_id).and_return(site)
        SiteBuilder.destroy_site_if_no_references(site_id, bookmark_id)
        expect(site).to have_received(:destroy)
      end
    end

    context 'when there is references to site' do
      it 'does nothing' do
        FactoryGirl.create(:bookmark, site_id: site_id)
        expect(Site).to_not receive(:find_by)
        SiteBuilder.destroy_site_if_no_references(site_id, bookmark_id)
      end
    end
  end
end
