# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TagBuilder do
  let(:tags) { 'cats,dogs' }
  let(:bookmark_id) { 9987 }

  describe '.call' do
    it 'initializes a new instance of TagBuilder and calls "call" on it' do
      builder = spy
      expect(TagBuilder).to receive(:new)
        .with(tags, bookmark_id).and_return(builder)
      TagBuilder.call(tags, bookmark_id)
      expect(builder).to have_received(:call)
    end
  end

  describe '#call' do
    it 'creates an association between tag and bookmark and remove old' do
      tag_1 = double(id: 34)
      tag_2 = double(id: 58)
      bookmark = FactoryGirl.create(:bookmark)
      tag = FactoryGirl.create(:tag, title: 'dogs_vs_cats')
      BookmarkTag.create(bookmark_id: bookmark.id, tag_id: tag.id)
      expect(Tag).to receive(:find_or_create_by).ordered
        .with(title: 'cats').and_return(tag_1)
      expect(BookmarkTag).to receive(:find_or_create_by).ordered
        .with(tag_id: tag_1.id, bookmark_id: bookmark.id)
      expect(Tag).to receive(:find_or_create_by).ordered
        .with(title: 'dogs').and_return(tag_2)
      expect(BookmarkTag).to receive(:find_or_create_by).ordered
        .with(tag_id: tag_2.id, bookmark_id: bookmark.id)
      builder = TagBuilder.new(tags, bookmark.id)
      builder.call
    end
  end
end
