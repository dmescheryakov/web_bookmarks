# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BookmarksController, type: :controller do
  let(:valid_attributes) do
    {
      title: 'Dogs on reddit.',
      url: 'https://reddit.com/dogs',
      shortening: 'dogs'
    }
  end

  let(:invalid_attributes) do
    {
      title: '',
      url: 'https://'
    }
  end

  describe 'GET #index' do
    it 'assigns all bookmarks as @bookmarks' do
      bookmark = FactoryGirl.create(:bookmark, valid_attributes)
      get :index, params: {}
      expect(assigns(:bookmarks)).to eq([bookmark])
    end
  end

  describe 'GET #show' do
    it 'assigns the requested bookmark as @bookmark' do
      bookmark = FactoryGirl.create(:bookmark, valid_attributes)
      get :show, params: { id: bookmark.to_param }
      expect(assigns(:bookmark)).to eq(bookmark)
    end
  end

  describe 'GET #new' do
    it 'assigns a new bookmark as @bookmark' do
      get :new, params: {}
      expect(assigns(:bookmark)).to be_a_new(Bookmark)
    end
  end

  describe 'GET #edit' do
    it 'assigns the requested bookmark as @bookmark' do
      bookmark = FactoryGirl.create(:bookmark, valid_attributes)
      get :edit, params: { id: bookmark.to_param }
      expect(assigns(:bookmark)).to eq(bookmark)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Bookmark' do
        expect do
          post :create, params: { bookmark: valid_attributes }
        end.to change(Bookmark, :count).by(1)
      end

      it 'assigns a newly created bookmark as @bookmark' do
        post :create, params: { bookmark: valid_attributes }
        expect(assigns(:bookmark)).to be_a(Bookmark)
        expect(assigns(:bookmark)).to be_persisted
      end

      it 'redirects to the root page' do
        post :create, params: { bookmark: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved bookmark as @bookmark' do
        post :create, params: { bookmark: invalid_attributes }
        expect(assigns(:bookmark)).to be_a_new(Bookmark)
      end

      it "re-renders the 'new' template" do
        post :create, params: { bookmark: invalid_attributes }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { { title: 'Cats on reddit' } }

      it 'updates the requested bookmark' do
        bookmark = FactoryGirl.create(:bookmark, valid_attributes)
        put :update, params: { id: bookmark.to_param, bookmark: new_attributes }
        bookmark.reload
        expect(bookmark.title).to eql('Cats on reddit')
      end

      it 'assigns the requested bookmark as @bookmark' do
        bookmark = FactoryGirl.create(:bookmark, valid_attributes)
        put :update, params: { id: bookmark.to_param, bookmark: valid_attributes }
        expect(assigns(:bookmark)).to eq(bookmark)
      end

      it 'redirects to the root page' do
        bookmark = FactoryGirl.create(:bookmark, valid_attributes)
        put :update, params: { id: bookmark.to_param, bookmark: valid_attributes }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'with invalid params' do
      it 'assigns the bookmark as @bookmark' do
        bookmark = FactoryGirl.create(:bookmark, valid_attributes)
        put :update, params: { id: bookmark.to_param, bookmark: invalid_attributes }
        expect(assigns(:bookmark)).to eq(bookmark)
      end

      it "re-renders the 'edit' template" do
        bookmark = FactoryGirl.create(:bookmark, valid_attributes)
        put :update, params: { id: bookmark.to_param, bookmark: invalid_attributes }
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested bookmark' do
      bookmark = FactoryGirl.create(:bookmark, valid_attributes)
      expect do
        delete :destroy, params: { id: bookmark.to_param }
      end.to change(Bookmark, :count).by(-1)
    end

    it 'redirects to the root path' do
      bookmark = FactoryGirl.create(:bookmark, valid_attributes)
      delete :destroy, params: { id: bookmark.to_param }
      expect(response).to redirect_to(root_path)
    end
  end
end
