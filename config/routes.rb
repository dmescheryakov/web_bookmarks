# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'bookmarks#index'
  resources :bookmarks
end
