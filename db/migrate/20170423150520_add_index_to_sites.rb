# frozen_string_literal: true

class AddIndexToSites < ActiveRecord::Migration[5.1]
  def change
    add_index :sites, :url
  end
end
