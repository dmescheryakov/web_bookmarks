# frozen_string_literal: true

class CreateBookmarkTags < ActiveRecord::Migration[5.1]
  def change
    create_table :bookmark_tags do |t|
      t.integer :bookmark_id
      t.integer :tag_id
      t.index %i[bookmark_id tag_id]

      t.timestamps
    end
  end
end
