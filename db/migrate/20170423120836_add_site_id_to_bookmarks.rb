# frozen_string_literal: true

class AddSiteIdToBookmarks < ActiveRecord::Migration[5.1]
  def change
    add_column :bookmarks, :site_id, :integer
  end
end
