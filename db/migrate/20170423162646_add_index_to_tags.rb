# frozen_string_literal: true

class AddIndexToTags < ActiveRecord::Migration[5.1]
  def change
    add_index :tags, :title
  end
end
