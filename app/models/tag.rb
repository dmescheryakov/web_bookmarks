# frozen_string_literal: true

class Tag < ApplicationRecord
  has_many :bookmark_tags
end
