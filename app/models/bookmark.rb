# frozen_string_literal: true

class Bookmark < ApplicationRecord
  validates :title, :url, presence: true
  validates_format_of :url, with: /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/?.*)\z/

  belongs_to :site, optional: true
  has_many :bookmark_tags
  has_many :tags, through: :bookmark_tags do
    def cache_key
      [count(:updated_at), maximum(:updated_at)].map(&:to_i).join('-')
    end
  end
end
