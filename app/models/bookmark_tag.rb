# frozen_string_literal: true

class BookmarkTag < ApplicationRecord
  belongs_to :bookmark, touch: true
  belongs_to :tag
end
