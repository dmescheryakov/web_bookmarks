# frozen_string_literal: true

class BookmarkFinder
  TEMPLATE = 'LOWER(title) LIKE :query OR LOWER(url) LIKE :query OR '\
             'LOWER(shortening) LIKE :query'

  def self.search(query)
    return [] unless query
    if query[:tag]
      search_by_tag(query)
    else
      Bookmark.includes(:tags)
              .where(TEMPLATE, query: "%#{query[:term].downcase}%")
    end
  end

  def self.search_by_tag(query)
    tag = query[:tag]
    return if tag.blank?
    Bookmark.joins(:tags).where(tags: { title: tag })
  end
end
