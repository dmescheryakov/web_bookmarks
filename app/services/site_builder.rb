# frozen_string_literal: true

class SiteBuilder
  def initialize(url, bookmark_id)
    @url = url
    @bookmark_id = bookmark_id
  end

  def self.call(url, bookmark_id)
    new(url, bookmark_id).call
  end

  def call
    site = Site.find_or_create_by(url: top_level_url)
    bookmark.update(site_id: site.id) if site
  end

  def self.destroy_site_if_no_references(site_id, bookmark_id)
    return if Bookmark.where(site_id: site_id).where.not(id: bookmark_id)
                      .count > 0
    site = Site.find_by(id: site_id)
    site.destroy
  end

  private

  def bookmark
    @bookmark ||= Bookmark.find(@bookmark_id)
  end

  def top_level_url
    uri = URI.parse(@url)
    "#{uri.scheme}://#{uri.host}/"
  end
end
