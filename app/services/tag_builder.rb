class TagBuilder
  def initialize(tags, bookmark_id)
    @tags = tags
    @bookmark_id = bookmark_id
  end

  def self.call(tags, bookmark_id)
    new(tags, bookmark_id).call
  end

  def call
    create_new_associations
    remove_old_associations
  end

  def remove_old_associations
    bookmark.tags.each do |tag|
      next if @tags && splited_tags.include?(tag.title)
      bookmark.bookmark_tags.where(tag_id: tag.id).destroy_all
      next if tag.bookmark_tags.count > 0
      tag.destroy
    end
  end

  private

  def create_new_associations
    return unless @tags
    splited_tags.each do |tag_title|
      tag = Tag.find_or_create_by(title: tag_title)
      BookmarkTag.find_or_create_by(tag_id: tag.id, bookmark_id: @bookmark_id)
    end
  end

  def splited_tags
    @splited_tags ||= @tags.split(',')
  end

  def bookmark
    Bookmark.includes(:tags).find(@bookmark_id)
  end
end
