# frozen_string_literal: true

class BookmarksController < ApplicationController
  before_action :set_bookmark, only: %i[show edit update destroy]

  # GET /bookmarks
  def index
    query = params[:q]
    @bookmarks = BookmarkFinder.search(query) if query
    @bookmarks ||= Bookmark.all.includes(:tags)
    @tags = Tag.all
  end

  # GET /bookmarks/1
  def show; end

  # GET /bookmarks/new
  def new
    @bookmark = Bookmark.new
  end

  # GET /bookmarks/1/edit
  def edit; end

  # POST /bookmarks
  def create
    @bookmark = Bookmark.new(bookmark_params)

    if @bookmark.save
      call_tag_builder
      perform_site_bilder_job
      redirect_to root_path, notice: 'Bookmark was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /bookmarks/1
  def update
    if @bookmark.update(bookmark_params)
      call_tag_builder
      perform_site_bilder_job
      redirect_to root_path, notice: 'Bookmark was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /bookmarks/1
  def destroy
    perform_site_destroyer_job
    remove_associations_with_tags
    @bookmark.destroy
    redirect_to root_path, notice: 'Bookmark was successfully destroyed.'
  end

  private

  def set_bookmark
    @bookmark = Bookmark.find(params[:id])
  end

  def bookmark_params
    params.fetch(:bookmark, {}).permit(:title, :url, :shortening)
  end

  def call_tag_builder
    TagBuilder.call(params[:tags], @bookmark.id)
  end

  def perform_site_bilder_job
    SiteBuilderJob.perform_later(@bookmark.url, @bookmark.id)
  end

  def perform_site_destroyer_job
    SiteDestroyerJob.perform_later(@bookmark.site_id, @bookmark.id)
  end

  def remove_associations_with_tags
    TagBuilder.new(nil, @bookmark.id).remove_old_associations
  end
end
