# frozen_string_literal: true

class SiteBuilderJob < ApplicationJob
  queue_as :default

  def perform(url, bookmark_id)
    SiteBuilder.call(url, bookmark_id)
  end
end
