# frozen_string_literal: true

class SiteDestroyerJob < ApplicationJob
  queue_as :default

  def perform(site_id, bookmark_id)
    SiteBuilder.destroy_site_if_no_references(site_id, bookmark_id)
  end
end
